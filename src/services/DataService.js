export const products = [
    {
        id:1,
        title: "product1",
        description: "descr1 Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        price: 15,
        photos: [
            {
                url: "https://picsum.photos/200"
            },
            {
                url: "https://picsum.photos/200"
            }
        ]
    },
    {
        id:2,
        title: "product2",
        description: "descr2 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        price: 150,
        photos: [
            {
                url: "https://picsum.photos/200"
            }
        ]
    },
]
export const favs = [
    {
        id: 1,
        products: [
            {
                id: 1
            }
        ]
    }
]

const users = [
    {
        id: 1,
        login : 'log1',
        password : 'test'
    },
    {
        id: 2,
        login : 'log2',
        password : 'test'
    }
]

export let indexProduct = products.length

export const modifierIndexProduct = (index) => {
    indexProduct = index
}

export const addProduct = (product) => {
    let lastIndex = indexProduct + 1
    let tmpProduct = {...product, id : lastIndex}
    products.push(tmpProduct)
    modifierIndexProduct(lastIndex)
}

export const addFav = (id) => {
    let tmpfavs = {...favs, id}
    favs.push(tmpfavs)
}

export const removeFav = (id) => {
    let tmpfav = favs.find(f=>f.id == id)
    favs.filter(f=> f != tmpfav)
}

export const getProductById = (id) => {
    return products.find(a=>a.id == id)
}

export let isLogged = false

export const login = (login, password) => {
    const u = users.find(l => l.login == login && l.password == password)
    return u != undefined
}
export const changeIsLogged = (log) => {
    isLogged = log
}