import React, { Component } from "react";
import {
  products
} from "./services/DataService";
import { ProductsListComponent } from "./products/ProductsListComponent"
import { SearchProductscomponent } from "./products/SearchProductsComponent";

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products : products
    };
  }

  search = (text) => {
    const tmpProducts = products.filter(p => p.title.includes(text) || p.description.includes(text))
    this.setState({
        products : tmpProducts
    })
}

  render() {
    return (
      <div className="container">
        <SearchProductscomponent products={this.state.products} search={this.search} />
        <ProductsListComponent history={this.props.history} products={this.state.products} search={this.search} />
      </div>
    );
  }
}
