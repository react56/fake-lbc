import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Home } from "./Home";
import { Add } from "./Add";
import { Login } from "./Login";
import { Favs } from "./Favs";
import { DetailProductComponent } from "./products/DetailProductComponent";
import {BrowserRouter, Switch, Route, Link} from 'react-router-dom'

function App() {
  return (
    <BrowserRouter>
      <div className="container">
          <nav className="navbar navbar-expand-lg navbar-dark bg-warning">
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item active">
                  <Link to="/">Accueil</Link>
                </li>
                <li className="nav-item active">
                  <Link to="/add">Ajouter Annonce</Link>
                </li>
                <li className="nav-item active">
                  <Link to="/favs">Mes Favoris</Link>
                </li>
              </ul>
            </div>
          </nav>
        <Switch>
              <Route path="/" exact component={Home}></Route>
              <Route path="/add/" exact component={Add}></Route>
              <Route path="/add/:id" component={Add}></Route>
              <Route path="/favs/" exact component={Favs}></Route>
              <Route path="/favs/:id" component={Favs}></Route>
              <Route path="/product-detail/:id" component={DetailProductComponent}></Route>
              <Route path="/login" component={Login}></Route>
            </Switch>
      </div>
    </BrowserRouter>
  );
}
export default App;
