import React, { Component } from "react";
import { addProduct, isLogged } from "./services/DataService";

export class Add extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: {
        title: "",
        description: "",
        price: "",
        photos: [{ url: "" }],
      },
      url: "",
    };
    {
      /* 
    if (!isLogged) {
      props.history.push("/login");
    }
*/
    }
  }

  changeField = (e) => {
    let tmpProduct = { ...this.state.product };
    let tmpPhoto = { ...this.state.url };
    if (e.target.getAttribute("name") == "url") {
      tmpPhoto = e.target.value;
      console.log(tmpPhoto)
    }

    tmpProduct[e.target.getAttribute("name")] = e.target.value;
    tmpProduct.photos[0].url = tmpPhoto
    this.setState({
      product: { ...tmpProduct },
      photo: { ...tmpPhoto },
    });
  };

  addProduct = () => {
    addProduct(this.state.product);
    this.props.history.push("/");
    this.setState({
      product: {
        title: "",
        description: "",
        price: "",
        photos: [{ url: "" }],
      },
      photo: {
        url: "",
      },
    });
  };

  render() {
    return (
      <div className="container mt-2 justify-content-center">
        <div className="col m-2">
          <input
            type="text"
            name="title"
            onChange={this.changeField}
            className="col-5 form-control"
            placeholder="Titre"
          />
        </div>
        <div className="col m-2">
          <textarea
            name="description"
            rows="5"
            className="col-5 form-control"
            onChange={this.changeField}
            placeholder="Votre description"
          ></textarea>
        </div>
        <div className="col m-2">
          <input
            type="number"
            name="price"
            onChange={this.changeField}
            className="col-5 form-control"
            placeholder="Prix"
          />
        </div>

        {this.state.product.photos.map((photo, idx) => (
          <div className="col m-2">
          <input
          type="text"
          name="url"
          onChange={this.changeField}
          className="col-5 form-control"
          placeholder={`url photo #${idx + 1}`}
        />
        </div>
        ))}

        <div className="col m-2">
          <button className="col-2 btn btn-warning" onClick={this.addProduct}>
            Ajouter
          </button>
        </div>
      </div>
    );
  }
}
