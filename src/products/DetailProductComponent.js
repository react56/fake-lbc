import React, { Component } from 'react';
import { getProductById } from '../services/DataService'

export class DetailProductComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            product : getProductById(props.match.params.id)
         }
    }
    render() { 
        return ( 
<div className="container bootstrap snippets bootdey">
    <div className="row">
        <div className="col-sm-6 push-bit">
            <a href="#" className="gallery-link"><img src={this.state.product.photos[0].url} alt="" className="img-responsive push-bit" /></a>
            <div className="row push-bit">
            </div>
        </div>
        <div className="col-sm-6 push-bit">
            <div className="clearfix">
                <div className="pull-right">
        <span className="h2"><strong>{this.state.product.price} €</strong></span>
                </div>
                <span className="h4">
                    <strong className="text-success">{this.state.product.title}</strong><br />
                </span>
            </div>
            <hr />
            {this.state.product.description}
            <hr />
        </div>
    </div>
</div>
         );
    }
}