import React, { Component } from "react";
import { DetailProductComponent } from "./DetailProductComponent";
import { ProductComponent } from './ProductComponent'


export class ProductsListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <>
        {this.props.products.map((product, index) => (
          <div className="row">
          <ProductComponent
            history={this.props.history}
            product={product}
            key={index}
          ></ProductComponent>
          </div>
        ))}
      </>
    );
  }
}
