import React, { Component } from "react";

export class ProductComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  reduceDescription = () => {
    const tmpDescription = this.props.product.description
    return tmpDescription.substring(0,51)
  }

  render() {
    return (
      <div className="row m-2 productContainer">
          <div className="col-2">
            <img id="imgHome" src={this.props.product.photos[0].url}></img>
          </div>
          <div className="col-2">
            {this.props.product.title} - {this.props.product.price} €
          </div>
          <div className="col-6">
            {this.reduceDescription()}
          </div>
        <div className="col-2">
          <button
            className="btn btn-warning"
            onClick={() => {
              this.props.history.push(
                "/product-detail/" + this.props.product.id
              );
            }}
          >
            Voir détail
          </button>
        </div>
      </div>
    );
  }
}
