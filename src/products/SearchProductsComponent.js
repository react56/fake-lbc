import React, { Component } from "react";

export class SearchProductscomponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div className="row m-1 justify-content-end">
        <input
          type="text"
          className="col-4 form-control"
          placeholder="Recherche"
          onChange={(e) => {
            this.props.search(e.target.value);
          }}
        />
      </div>
    );
  }
}
